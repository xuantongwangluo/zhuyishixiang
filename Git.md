###git设置用户名和密码

下面的这两行命令就是设置用户名和email：
```shell
$ git config --global user.name author #将用户名设为author
$ git config --global user.email author@corpmail.com #将用户邮箱设为author@corpmail.com
```
### 生成SSH Key

你需要把邮件地址换成你自己的邮件地址，然后一路回车，使用默认值即可，由于这个Key也不是用于军事目的，所以也无需设置密码。
如果一切顺利的话，可以在用户主目录里找到`.ssh`目录，里面有`id_rsa`和`id_rsa.pub`两个文件，这两个就是`SSH Key`的秘钥对，`id_rsa`是私钥，不能泄露出去，`id_rsa.pub`是公钥，可以放心地告诉任何人。
```
$ ssh-keygen -t rsa -C "youremail@example.com"
```
### 远程仓库克隆到本地

```
git clone ...
```

### 强制覆盖已有的分支

```
git push -u origin master -f
```

### 可以通过如下命令进行代码合并【注：pull=fetch+merge]

```
git pull --rebase origin master
```

### 添加远程仓库

```
git remote add pb git://github.com/paulboone/ticgit.git
```
