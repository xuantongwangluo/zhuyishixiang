/*
Navicat MySQL Data Transfer

Source Server         : mydatabase
Source Server Version : 50714
Source Host           : localhost:3306
Source Database       : tanji

Target Server Type    : MYSQL
Target Server Version : 50714
File Encoding         : 65001

Date: 2017-07-07 18:26:25
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for tj_admin
-- ----------------------------
DROP TABLE IF EXISTS `tj_admin`;
CREATE TABLE `tj_admin` (
  `id` int(11) NOT NULL,
  `accname` varchar(255) DEFAULT NULL,
  `pwd` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tj_admin
-- ----------------------------

-- ----------------------------
-- Table structure for tj_order
-- ----------------------------
DROP TABLE IF EXISTS `tj_order`;
CREATE TABLE `tj_order` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `money` float DEFAULT NULL,
  `done` tinyint(4) DEFAULT '0' COMMENT '是否制作完成，默认0. 制作完成之后需要置1.表示不在提醒管理员制作',
  `paytime` datetime DEFAULT NULL COMMENT '支付时间',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tj_order
-- ----------------------------

-- ----------------------------
-- Table structure for tj_order_list
-- ----------------------------
DROP TABLE IF EXISTS `tj_order_list`;
CREATE TABLE `tj_order_list` (
  `id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL COMMENT '属于哪个订单',
  `tea_id` int(11) NOT NULL COMMENT '奶茶id',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tj_order_list
-- ----------------------------

-- ----------------------------
-- Table structure for tj_tea
-- ----------------------------
DROP TABLE IF EXISTS `tj_tea`;
CREATE TABLE `tj_tea` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type_id` int(11) NOT NULL,
  `teaname` varchar(255) DEFAULT NULL COMMENT '奶茶名称',
  `price` float DEFAULT NULL,
  `brief` varchar(255) DEFAULT NULL COMMENT '简介',
  `picture` varchar(255) DEFAULT NULL COMMENT '图片路径',
  `empty` tinyint(4) DEFAULT '0' COMMENT '是否售空；空是1，有货为0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tj_tea
-- ----------------------------

-- ----------------------------
-- Table structure for tj_type
-- ----------------------------
DROP TABLE IF EXISTS `tj_type`;
CREATE TABLE `tj_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `typename` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tj_type
-- ----------------------------

-- ----------------------------
-- Table structure for tj_user
-- ----------------------------
DROP TABLE IF EXISTS `tj_user`;
CREATE TABLE `tj_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '账号，用于登录',
  `acc_num` varchar(255) DEFAULT NULL,
  `pwd` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL COMMENT '名字，即称呼',
  `phone` varchar(255) DEFAULT NULL,
  `vip` tinyint(4) DEFAULT NULL COMMENT '1表示会员  0是非付费会员',
  `reg_date` datetime DEFAULT NULL COMMENT '注册时间',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tj_user
-- ----------------------------

-- ----------------------------
-- Table structure for tj_video
-- ----------------------------
DROP TABLE IF EXISTS `tj_video`;
CREATE TABLE `tj_video` (
  `id` int(11) NOT NULL,
  `videoname` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL COMMENT '标题',
  `brief` varchar(255) DEFAULT NULL COMMENT '简介',
  `url` varchar(255) DEFAULT NULL COMMENT '网址',
  `uploaddate` datetime DEFAULT NULL COMMENT '上传日期',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tj_video
-- ----------------------------
